# TinyEdit

Text editor for Thumby console

## Status
TinyEdit is being re-written. Some parts are broken while this happens. The version available on the Thumby arcade works, however.
Editing files is possible (I've been using it), but there may bugs, and you *might* lose data or even brick your Thumby (if you edit the wrong file).
Useful editing is limited by the tiny screen size of the Thumby, and by battery life.

## Motivation
I worked on a Thumby game that used text files to define levels. While play testing, I found a problem in a level. I couldn't fix it on the Thumby - I had to connect it to my computer.

"What I need is a text editor that runs on the Thumby!"

So I made one.

## Version note
This version of TinyEdit announces itself as `2`. The version currently available from the Thumby arcade doesn't announce a version, but it's version `1`. The difference is that v2 can cope with arbitrarily large files by only holding part of the open file in memory, and keeping the rest on disk.

## Help
When you've opened a file, you can scroll around with the D-pad.

To edit the current line, press B.
Or for a menu, press A.

### Editing a line
The keyboard fills most of the screen, and the line being edited is at the bottom. There's a text cursor to show where typed characters will appear.

Use the D-pad to select a key on the keyboard, then press A to type it.

To move the cursor, type the left or right arrow key on the bottom row of the keyboard.

The keyboard has several layers. Type the shift key (bottom left of the keyboard) to cycle between them.

There are delete buttons for a character and a chunk on each keyboard layer. "Chunk" means a sequence of letters, or a sequence of non-letter characters.

One keyboard layer has a whole row of delete buttons that delete in either direction, and to either end of a line.

When you've finished editing a line, press B. There is no undo! If you've make a mistake, either correct it or reload the file.

### Moving a line
On the "Line..." menu, select "Move" and you can move the current line up or down. Press A or B when the line is in the right place.

### Splitting a line
On the "Line..." menu, select "Split" and the current line is divided in two. Use the left and right buttons to move the split point to where you want, and press A or B to complete the split.

## FAQ
### It died! WTF?
If something goes wrong (say, the Thumby is out of memory) and the editor dies, it will try to write details of the problem to `/Games/TinyEdit/crashdump.log` and also to the screen. Press A or B to restart the Thumby.

If you see errors like this, please send me the `crashdump.log` file.

### What encoding does it use?
Apologies if English-isn't your language: there isn't any way to enter non-ASCII characters. I've specified UTF-8 for reading and writing files, so any existing non-ASCII characters *should* be preserved (but I suspect that they're *not*). And if you do load any non-ASCII characters, I'm not sure if the Thumby fonts support them. One curiosity is that the character `~` displays as a small arrow.

### Why doesn't it...?
Version 1 of TinyEdit loaded a whole file into memory to edit it. The Thumby has very limited memory, so large files would crash the editor, hence the error handling described above. This also limited the feature set that I could implement: more editor code in memory meant it would only load smaller files. Some features that were considered but not implemented were:
* replace
* regex find
* line number display

But now that memory issues have been addressed, future versions of TinyEdit might implement more features.
