import unittest
from FileChunks import FileChunk, ChunkProblem
from os import listdir, remove, mkdir, rmdir, path

class FileChunkTest( unittest.TestCase ):
	def tearDown( self ):
		FileChunk.CHUNK_PATH = './tmp'
		if not path.exists( FileChunk.CHUNK_PATH ):
			return
		for f in listdir( FileChunk.CHUNK_PATH ):
			remove( FileChunk.CHUNK_PATH + '/' + f )
		rmdir( FileChunk.CHUNK_PATH )

	def prepareTmpDir( self ):
		FileChunk.CHUNK_PATH = './tmp'
		if not path.exists( FileChunk.CHUNK_PATH ):
			mkdir( FileChunk.CHUNK_PATH )

	def testCtor( self ):
		# When
		chnk = FileChunk( [ 'Index 0', 'Index 1', 'Index 2' ], 0 )

		# Then
		self.assertEqual( 3,    chnk.lineCount() )
		self.assertEqual( None, chnk.name        )
		self.assertEqual( 0,    chnk.startIndex  )

		# And when
		chnk = FileChunk( [ 'Index 0' ], 99 )

		# Then
		self.assertEqual( 1,    chnk.lineCount() )
		self.assertEqual( None, chnk.name        )
		self.assertEqual( 99,   chnk.startIndex  )

		# And when
		chnk = FileChunk( [], -1 )

		# Then
		self.assertEqual( 0,    chnk.lineCount() )
		self.assertEqual( None, chnk.name        )
		self.assertEqual( -1,   chnk.startIndex  )

	def testGetLine( self ):
		# When
		chnk = FileChunk( [ 'Index 3', 'Index 4', 'Index 5' ], 3 )

		# Then
		self.assertEqual( 'Index 4', chnk.getLine( 4 ).text )
		self.assertEqual( 'Index 5', chnk.getLine( 5 ).text )
		try:
			chnk.getLine( 1 )
			self.fail( "Unexpected lack of exception for line 1" )
		except ChunkProblem as x:
			pass

	def testDropContent( self ):
		# When
		chnk = FileChunk( [ 'Index 0', 'Index 1', 'Index 2' ], 0 )

		# Then
		self.assertEqual( 3, chnk.lineCount() )
		self.assertEqual( 3, len( chnk.lines ) )

		# And when
		chnk.dropContent()

		# Then
		self.assertEqual( 3, chnk.lineCount() )
		self.assertEqual( 0, len( chnk.lines ) )

	def testWriteChunk( self ):
		# Given
		self.prepareTmpDir()
		chnk = FileChunk( [ 'aa', 'bbb', 'cccc' ], 77 )

		# When
		chnk.write( 'test' );

		# Then
		self.assertTrue( path.exists( FileChunk.CHUNK_PATH + '/test.chunk' ) )
		self.assertEqual( 12, path.getsize( FileChunk.CHUNK_PATH + '/test.chunk' ) )

	def testSetNameGetPath( self ):
		chnk = FileChunk( [ 'qaz', 'wsx', 'edc' ], 24 )
		chnk.setName( 'rfv' )
		self.assertEqual( FileChunk.CHUNK_PATH + '/rfv.chunk', chnk.getPath() )

	def testLastIndex( self ):
		chnk = FileChunk( [ '12345', 'qwerty', 'asdf', 'zxcv' ], 17 )
		self.assertEqual( 20, chnk.lastIndex() )

	def testIncludes( self ):
		chnk = FileChunk( [ '34', '35', '36' ], 34 )
		self.assertFalse( chnk.includes( 33 ) )
		self.assertTrue ( chnk.includes( 34 ) )
		self.assertTrue ( chnk.includes( 35 ) )
		self.assertTrue ( chnk.includes( 36 ) )
		self.assertFalse( chnk.includes( 37 ) )

	def testNearTop( self ):
		chnk = FileChunk( [ '91', '92', '93', '94' ], 91 )
		self.assertTrue ( chnk.nearTop( 90 ) )
		self.assertTrue ( chnk.nearTop( 91 ) )
		self.assertTrue ( chnk.nearTop( 92 ) )
		self.assertFalse( chnk.nearTop( 93 ) )
		self.assertFalse( chnk.nearTop( 94 ) )
		self.assertFalse( chnk.nearTop( 95 ) )

	def testNearBottom( self ):
		chnk = FileChunk( [ '91', '92', '93', '94' ], 91 )
		self.assertFalse( chnk.nearBottom( 90 ) )
		self.assertFalse( chnk.nearBottom( 91 ) )
		self.assertFalse( chnk.nearBottom( 92 ) )
		self.assertTrue ( chnk.nearBottom( 93 ) )
		self.assertTrue ( chnk.nearBottom( 94 ) )
		self.assertTrue ( chnk.nearBottom( 95 ) )

	def testWriteChunk( self ):
		# Given
		self.prepareTmpDir()
		chnk = FileChunk( [ 'aa', 'bbb', 'cccc' ], 77 )
		chnk.dropContent()
		chnk.setName( 'test' )
		with open( chnk.getPath(), 'w', encoding = 'utf-8' ) as f:
			f.write( 'xx\nyyy\nzzzz\n' )

		# When
		chnk.read();

		# Then
		with self.assertRaises(ChunkProblem):
			chnk.getLine( 76 )
		self.assertEqual( 'xx',   chnk.getLine( 77 ).text )
		self.assertEqual( 'yyy',  chnk.getLine( 78 ).text )
		self.assertEqual( 'zzzz', chnk.getLine( 79 ).text )
		with self.assertRaises(ChunkProblem):
			chnk.getLine( 80 )

	def testReplaceLine( self ):
		# Given
		self.prepareTmpDir()
		chnk = FileChunk( [ 'index 34', 'index 35', 'index 36' ], 34 )
		self.assertEqual( 3, chnk.lineCount() )
		chnk.setName( 'test' )
		chnk.write()

		# Then
		with open( chnk.getPath(), 'r', encoding = 'utf-8' ) as f:
			self.assertEqual( 'index 35', f.read().splitlines()[ 1 ] )

		# And when
		chnk.replaceLine( 35, 'tgb' );

		# Then
		self.assertEqual( 3, chnk.lineCount() )
		with open( chnk.getPath(), 'r', encoding = 'utf-8' ) as f:
			self.assertEqual( 'tgb', f.read().splitlines()[ 1 ] )
		self.assertEqual( 'tgb', chnk.getLine( 35 ).text )

	def testWriteTo( self ):
		self.prepareTmpDir()
		fileName = FileChunk.CHUNK_PATH + '/testWriteToChunk.txt'
		chnk = FileChunk( [ 'cat', 'dog', 'fish' ], 22 )
		with open( fileName, 'w', encoding = 'utf-8' ) as f:
			chnk.writeTo( f )
		with open( fileName, 'r', encoding = 'utf-8' ) as f:
			lines = f.read().splitlines()
			self.assertEqual( 3, len( lines ) )
			self.assertEqual( 'cat',  lines[ 0 ] )
			self.assertEqual( 'dog',  lines[ 1 ] )
			self.assertEqual( 'fish', lines[ 2 ] )

	def testDuplicateLine( self ):
		# Given
		self.prepareTmpDir()
		chnk = FileChunk( [ 'Ack', 'Beer', 'Charlie' ], 17 )
		self.assertEqual( 17, chnk.startIndex )
		self.assertEqual( 19, chnk.lastIndex() )
		chnk.setName( 'test' )
		chnk.write()
		with open( chnk.getPath(), 'r', encoding = 'utf-8' ) as f:
			self.assertEqual( 3, len( f.read().splitlines() ) )

		# When
		chnk.duplicateLine( 18 );

		# Then
		self.assertEqual( 4, chnk.lineCount() )
		self.assertEqual( 'Ack',     chnk.getLine( 17 ).text )
		self.assertEqual( 'Beer',    chnk.getLine( 18 ).text )
		self.assertEqual( 'Beer',    chnk.getLine( 19 ).text )
		self.assertEqual( 'Charlie', chnk.getLine( 20 ).text )
		self.assertEqual( 17, chnk.startIndex )
		self.assertEqual( 20, chnk.lastIndex() )
		with open( chnk.getPath(), 'r', encoding = 'utf-8' ) as f:
			lines = f.read().splitlines()
			self.assertEqual( 4, len( lines ) )
			self.assertEqual( 'Ack',     lines[ 0 ] )
			self.assertEqual( 'Beer',    lines[ 1 ] )
			self.assertEqual( 'Beer',    lines[ 2 ] )
			self.assertEqual( 'Charlie', lines[ 3 ] )

	def testDeleteLine( self ):
		# Given
		self.prepareTmpDir()
		chnk = FileChunk( [ 'Ack', 'Beer', 'Charlie' ], 17 )
		self.assertEqual( 17, chnk.startIndex )
		self.assertEqual( 19, chnk.lastIndex() )
		chnk.setName( 'test' )
		chnk.write()
		with open( chnk.getPath(), 'r', encoding = 'utf-8' ) as f:
			self.assertEqual( 3, len( f.read().splitlines() ) )

		# When
		deletedLineText = chnk.deleteLine( 18 );

		# Then
		self.assertEqual( 'Beer', deletedLineText )
		self.assertEqual( 2, chnk.lineCount() )
		self.assertEqual( 'Ack',     chnk.getLine( 17 ).text )
		self.assertEqual( 'Charlie', chnk.getLine( 18 ).text )
		self.assertEqual( 17, chnk.startIndex )
		self.assertEqual( 18, chnk.lastIndex() )
		with open( chnk.getPath(), 'r', encoding = 'utf-8' ) as f:
			lines = f.read().splitlines()
			self.assertEqual( 2, len( lines ) )
			self.assertEqual( 'Ack',     lines[ 0 ] )
			self.assertEqual( 'Charlie', lines[ 1 ] )

	def testAdjustIndexes( self ):
		# Given
		chnk = FileChunk( [ 'Tom', 'Disk', 'Harry' ], 42 )
		self.assertEqual( 42, chnk.startIndex )
		self.assertEqual( 44, chnk.lastIndex() )

		# When
		chnk.adjustIndexes( 1 )	

		# Then
		self.assertEqual( 43, chnk.startIndex )
		self.assertEqual( 45, chnk.lastIndex() )

		# When
		chnk.adjustIndexes( -10 )

		# Then
		self.assertEqual( 33, chnk.startIndex )
		self.assertEqual( 35, chnk.lastIndex() )

	def testMoveUp( self ):
		# Given
		self.prepareTmpDir()
		chnk = FileChunk( [ 'Jupiter', 'Juno', 'Minerva' ], 71 )
		chnk.setName( 'test' )
		self.assertEqual( 71, chnk.startIndex )
		self.assertEqual( 73, chnk.lastIndex() )

		# When
		chnk.moveUp( 73 )

		# Then
		self.assertEqual( 'Jupiter', chnk.getLine( 71 ).text )
		self.assertEqual( 'Minerva', chnk.getLine( 72 ).text )
		self.assertEqual( 'Juno',    chnk.getLine( 73 ).text )
		self.assertEqual( 71, chnk.startIndex )
		self.assertEqual( 73, chnk.lastIndex() )

	def testMoveDown( self ):
		# Given
		self.prepareTmpDir()
		chnk = FileChunk( [ 'Odin', 'Thor', 'Tiu' ], 61 )
		chnk.setName( 'test' )
		self.assertEqual( 61, chnk.startIndex )
		self.assertEqual( 63, chnk.lastIndex() )

		# When
		chnk.moveDown( 62 )

		# Then
		self.assertEqual( 'Odin', chnk.getLine( 61 ).text )
		self.assertEqual( 'Tiu',  chnk.getLine( 62 ).text )
		self.assertEqual( 'Thor', chnk.getLine( 63 ).text )
		self.assertEqual( 61, chnk.startIndex )
		self.assertEqual( 63, chnk.lastIndex() )

	def testAppendLine( self ):
		# Given
		self.prepareTmpDir()
		chnk = FileChunk( [ 'Zeus', 'Hera', 'Athena' ], 81 )
		chnk.setName( 'test' )
		self.assertEqual( 81, chnk.startIndex )
		self.assertEqual( 83, chnk.lastIndex() )

		# When
		chnk.appendLine( 'Aphrodite' )

		# Then
		self.assertEqual( 4, chnk.lineCount() )
		self.assertEqual( 'Zeus',      chnk.getLine( 81 ).text )
		self.assertEqual( 'Hera',      chnk.getLine( 82 ).text )
		self.assertEqual( 'Athena',    chnk.getLine( 83 ).text )
		self.assertEqual( 'Aphrodite', chnk.getLine( 84 ).text )

	def testInsertLine( self ):
		# Given
		self.prepareTmpDir()
		chnk = FileChunk( [ 'Groucho', 'Chico', 'Harpo' ], 101 )
		chnk.setName( 'test' )
		self.assertEqual( 101, chnk.startIndex )
		self.assertEqual( 103, chnk.lastIndex() )

		# When
		chnk.insertLine( 102, 'Zeppo' )

		# Then
		self.assertEqual( 4, chnk.lineCount() )
		self.assertEqual( 'Groucho', chnk.getLine( 101 ).text )
		self.assertEqual( 'Zeppo',   chnk.getLine( 102 ).text )
		self.assertEqual( 'Chico',   chnk.getLine( 103 ).text )
		self.assertEqual( 'Harpo',   chnk.getLine( 104 ).text )
		self.assertEqual( 101, chnk.startIndex )
		self.assertEqual( 104, chnk.lastIndex() )

if __name__ == '__main__':
	unittest.main()
