import unittest
from os import listdir, remove, mkdir, rmdir, path
from FileChunks import FileChunk, FileChunks, ChunkProblem

def dumpChunks( chunks ):
	print( 'chunk count:',      len( chunks.chunkList ) )
	print( 'topChunkIndex:',    chunks.topChunkIndex    )
	print( 'bottomChunkIndex:', chunks.bottomChunkIndex )
	print( 'currentLineIndex:', chunks.currentLineIndex )
	for i, chunk in enumerate( chunks.chunkList ):
		if i == chunks.topChunkIndex:
			print( '  # Top chunk' )
		if i == chunks.bottomChunkIndex:
			print( '  # Bottom chunk' )
		print( '  name:', chunk.name )
		print( '  startIndex:', chunk.startIndex )
		print( '  lnCount:', chunk.lnCount )
		print( '  lastIndex():', chunk.lastIndex() )
		print( '  in memory:', not not chunk.lines )
		#for j, line in enumerate( chunk.lines ):
		#	print( j + chunk.startIndex, line.text )
		print()

class FileChunksTest( unittest.TestCase ):
	def tearDown( self ):
		FileChunk.CHUNK_PATH = './tmp'
		if not path.exists( FileChunk.CHUNK_PATH ):
			return
		for f in listdir( FileChunk.CHUNK_PATH ):
			remove( FileChunk.CHUNK_PATH + '/' + f )
		rmdir( FileChunk.CHUNK_PATH )

	def setUp( self ):
		FileChunk.CHUNK_PATH = './tmp'
		if not path.exists( FileChunk.CHUNK_PATH ):
			try:
				mkdir( FileChunk.CHUNK_PATH )
			except Exception:
				pass
		self.chunks = FileChunks( 'testfile.txt' )

	def checkChunkCount( self, expected ):
		self.assertEqual( expected, sum( [ len( chunk.lines ) > 0 for chunk in self.chunks.chunkList ] ) )

	def testCtor( self ):
		self.assertEqual( 4, len( self.chunks.chunkList ) )
		files = listdir( FileChunk.CHUNK_PATH )
		self.assertEqual( 4, len( files ) )
		self.assertEqual( '0.chunk', files[ 0 ] )
		self.assertEqual( '1.chunk', files[ 1 ] )
		self.assertEqual( '2.chunk', files[ 2 ] )
		self.assertEqual( '3.chunk', files[ 3 ] )
		self.assertEqual( './tmp/0.chunk', self.chunks.chunkList[ 0 ].getPath() )
		self.assertEqual( './tmp/1.chunk', self.chunks.chunkList[ 1 ].getPath() )
		self.assertEqual( './tmp/2.chunk', self.chunks.chunkList[ 2 ].getPath() )
		self.assertEqual( './tmp/3.chunk', self.chunks.chunkList[ 3 ].getPath() )

		self.assertEqual( 4, len( self.chunks.chunkList ) )

		self.assertEqual(  0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 49, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 48, self.chunks.chunkList[ 0 ].lastIndex() )

		self.assertEqual( 49, self.chunks.chunkList[ 1 ].startIndex )
		self.assertEqual( 22, self.chunks.chunkList[ 1 ].lineCount() )
		self.assertEqual( 70, self.chunks.chunkList[ 1 ].lastIndex() )

		self.assertEqual( 71, self.chunks.chunkList[ 2 ].startIndex )
		self.assertEqual( 18, self.chunks.chunkList[ 2 ].lineCount() )
		self.assertEqual( 88, self.chunks.chunkList[ 2 ].lastIndex() )

		self.assertEqual( 89, self.chunks.chunkList[ 3 ].startIndex )
		self.assertEqual( 10, self.chunks.chunkList[ 3 ].lineCount() )
		self.assertEqual( 98, self.chunks.chunkList[ 3 ].lastIndex() )

		self.checkChunkCount( 2 )

	def testLineCount( self ):
		self.assertEqual( 99, self.chunks.lineCount() )

	def testGetLine( self ):
		self.assertEqual( ' List 1',      self.chunks.getLine(  5 ).getText(  0 ) )
		self.assertEqual( '<ish swam i>', self.chunks.getLine( 35 ).getText( 10 ) )
		with self.assertRaises(ChunkProblem):
			self.chunks.getLine( 98 )

		self.checkChunkCount( 2 )

	def testGetCurrentLineIndex( self ):
		self.assertEqual( 0, self.chunks.getCurrentLineIndex() )
		self.chunks.setCurrentLineIndex( 98 )
		self.assertEqual( 98, self.chunks.getCurrentLineIndex() )
		self.chunks.setCurrentLineIndex( 5 )
		self.assertEqual( 5, self.chunks.getCurrentLineIndex() )

		self.checkChunkCount( 2 )

	def testSetCurrentLineIndex( self ):
		# First test that everything started normally at line 0
		self.assertEqual( 0, self.chunks.topChunkIndex )
		self.assertEqual( 1, self.chunks.bottomChunkIndex )
		# The first 2 chunks should have content
		self.assertTrue( 0 < len( self.chunks.chunkList[ 0 ].lines ) )
		self.assertTrue( 0 < len( self.chunks.chunkList[ 1 ].lines ) )
		# But the others shouldn't
		self.assertEqual( 0, len( self.chunks.chunkList[ 2 ].lines ) )
		self.assertEqual( 0, len( self.chunks.chunkList[ 3 ].lines ) )
		# Lines in the first to chunks should be available
		self.testGetLine()

		self.checkChunkCount( 2 )

		# If we move forward only a little, nothing should have changed
		self.chunks.setCurrentLineIndex( 10 )
		self.assertEqual( 0, self.chunks.topChunkIndex )
		self.assertEqual( 1, self.chunks.bottomChunkIndex )
		self.assertTrue( 0 < len( self.chunks.chunkList[ 0 ].lines ) )
		self.assertTrue( 0 < len( self.chunks.chunkList[ 1 ].lines ) )
		self.assertEqual( 0, len( self.chunks.chunkList[ 2 ].lines ) )
		self.assertEqual( 0, len( self.chunks.chunkList[ 3 ].lines ) )
		self.testGetLine()

		self.checkChunkCount( 2 )

		# If we move to the end, chunks should be 2 and 3
		self.chunks.setCurrentLineIndex( 98 )
		self.assertEqual( 2, self.chunks.topChunkIndex )
		self.assertEqual( 3, self.chunks.bottomChunkIndex )
		self.assertEqual( 0, len( self.chunks.chunkList[ 0 ].lines ) )
		self.assertEqual( 0, len( self.chunks.chunkList[ 1 ].lines ) )
		self.assertTrue( 0 < len( self.chunks.chunkList[ 2 ].lines ) )
		self.assertTrue( 0 < len( self.chunks.chunkList[ 3 ].lines ) )
		# And line 98 should be available
		self.assertTrue( self.chunks.chunkList[ 3 ].includes( 98 ) )
		self.assertEqual( '<a man more>', self.chunks.getLine( 98 ).getText( 10 ) )
		# But not lines near the start
		with self.assertRaises(ChunkProblem):
			self.chunks.getLine( 5 )

		# If we go back to the start, everything should be back how it started out.
		self.chunks.setCurrentLineIndex( 0 )
		self.assertEqual( 0, self.chunks.topChunkIndex )
		self.assertEqual( 1, self.chunks.bottomChunkIndex )
		self.assertTrue( 0 < len( self.chunks.chunkList[ 0 ].lines ) )
		self.assertTrue( 0 < len( self.chunks.chunkList[ 1 ].lines ) )
		self.assertEqual( 0, len( self.chunks.chunkList[ 2 ].lines ) )
		self.assertEqual( 0, len( self.chunks.chunkList[ 3 ].lines ) )
		self.testGetLine()

		self.checkChunkCount( 2 )

		# Line 69 should be near the bottom of of the bottom chunk
		self.assertTrue( self.chunks.chunkList[ self.chunks.topChunkIndex ].nearBottom( 69 ) )

		# If we go near the bottom of the bottom chunk, we should shuffle downward by one chunk
		self.chunks.setCurrentLineIndex( 69 )
		self.assertEqual( 1, self.chunks.topChunkIndex )
		self.assertEqual( 2, self.chunks.bottomChunkIndex )
		self.assertEqual( 0, len( self.chunks.chunkList[ 0 ].lines ) )
		self.assertTrue( 0 < len( self.chunks.chunkList[ 1 ].lines ) )
		self.assertTrue( 0 < len( self.chunks.chunkList[ 2 ].lines ) )
		self.assertEqual( 0, len( self.chunks.chunkList[ 3 ].lines ) )
		# And line 69 should still be available
		self.assertTrue( self.chunks.chunkList[ 1 ].includes( 69 ) )
		self.assertEqual( '<light thro>', self.chunks.getLine( 69 ).getText( 10 ) )
		# But so should line 71
		self.assertTrue( self.chunks.chunkList[ 2 ].includes( 71 ) )
		self.assertEqual( '<ds die man>', self.chunks.getLine( 71 ).getText( 10 ) )
		# But not lines near the start...
		with self.assertRaises(ChunkProblem):
			self.chunks.getLine( 4 )
		# ...or near the end
		with self.assertRaises(ChunkProblem):
			self.chunks.getLine( 97 )

		self.checkChunkCount( 2 )

		# If we move to the end, then near the beginning of the top chunk, we should shuffle up by one chunk
		self.chunks.setCurrentLineIndex( 98 )
		self.assertTrue( self.chunks.chunkList[ self.chunks.topChunkIndex ].nearTop( 72 ) )
		self.chunks.setCurrentLineIndex( 72 )
		self.assertEqual( 1, self.chunks.topChunkIndex )
		self.assertEqual( 2, self.chunks.bottomChunkIndex )
		self.assertEqual( 0, len( self.chunks.chunkList[ 0 ].lines ) )
		self.assertTrue( 0 < len( self.chunks.chunkList[ 1 ].lines ) )
		self.assertTrue( 0 < len( self.chunks.chunkList[ 2 ].lines ) )
		self.assertEqual( 0, len( self.chunks.chunkList[ 3 ].lines ) )

		self.checkChunkCount( 2 )

	def testGetCurrentLine( self ):
		self.chunks.setCurrentLineIndex( 5 )
		self.assertEqual( 'List 1', self.chunks.getCurrentLine().text )
		self.chunks.setCurrentLineIndex( 6 )
		self.assertEqual( 'The birch canoe slid on the smooth planks.', self.chunks.getCurrentLine().text )
		self.chunks.setCurrentLineIndex( 98 )
		self.assertEqual( "51. 'I am a man more sinned against than sinning.' (King Lear, Act 3, Scene 2)", self.chunks.getCurrentLine().text )

		self.checkChunkCount( 2 )

	def testReplaceCurrentLine( self ):
		self.chunks.setCurrentLineIndex( 5 )
		self.assertEqual( 'List 1', self.chunks.getCurrentLine().text )
		with open( self.chunks.chunkList[ 0 ].getPath(), 'r', encoding = 'utf-8' ) as f:
			self.assertEqual( 'List 1', f.read().splitlines()[ 5 ] )
		self.chunks.replaceCurrentLine( 'List One' )
		self.assertEqual( 'List One', self.chunks.getCurrentLine().text )
		with open( self.chunks.chunkList[ 0 ].getPath(), 'r', encoding = 'utf-8' ) as f:
			self.assertEqual( 'List One', f.read().splitlines()[ 5 ] )

		self.checkChunkCount( 2 )

	def testWriteTo( self ):
		# Given
		self.chunks.setCurrentLineIndex( 5 )
		self.chunks.replaceCurrentLine( 'List One' )
		self.chunks.setCurrentLineIndex( 98 )
		self.chunks.replaceCurrentLine( 'Bard out' )
		testFileName = FileChunk.CHUNK_PATH + '/testWriteTo.txt'

		# When
		with open( testFileName, 'w', encoding = 'utf-8' ) as f:
			self.chunks.writeTo( f )

		# Then
		with open( testFileName, 'r', encoding = 'utf-8' ) as f:
			lines = f.read().splitlines()
			self.assertEqual( 99, len( lines ) )
			self.assertEqual( 'List One', lines[ 5 ] )
			self.assertEqual( 'Bard out', lines[ 98 ] )

		self.checkChunkCount( 2 )

	def testDuplicateCurrentLine( self ):
		# Given
		self.chunks.setCurrentLineIndex( 5 )
		self.assertEqual( 'List 1', self.chunks.getCurrentLine().text )
		self.chunks.setCurrentLineIndex( 6 )
		self.assertEqual( 'The birch canoe slid on the smooth planks.', self.chunks.getCurrentLine().text )

		# When
		self.chunks.setCurrentLineIndex( 5 )
		self.chunks.duplicateCurrentLine()

		# Then
		self.assertEqual( 100, self.chunks.lineCount() )
		self.chunks.setCurrentLineIndex( 5 )
		self.assertEqual( 'List 1', self.chunks.getCurrentLine().text )
		self.chunks.setCurrentLineIndex( 6 )
		self.assertEqual( 'List 1', self.chunks.getCurrentLine().text )
		self.chunks.setCurrentLineIndex( 7 )
		self.assertEqual( 'The birch canoe slid on the smooth planks.', self.chunks.getCurrentLine().text )

		self.checkChunkCount( 2 )

		# This chunk should have got longer
		self.assertEqual(  0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 50, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 49, self.chunks.chunkList[ 0 ].lastIndex() )

		# Other chunks' line numbers should have adjusted
		self.assertEqual( 50, self.chunks.chunkList[ 1 ].startIndex )
		self.assertEqual( 22, self.chunks.chunkList[ 1 ].lineCount() )
		self.assertEqual( 71, self.chunks.chunkList[ 1 ].lastIndex() )

		self.assertEqual( 72, self.chunks.chunkList[ 2 ].startIndex )
		self.assertEqual( 18, self.chunks.chunkList[ 2 ].lineCount() )
		self.assertEqual( 89, self.chunks.chunkList[ 2 ].lastIndex() )

		self.assertEqual( 90, self.chunks.chunkList[ 3 ].startIndex )
		self.assertEqual( 10, self.chunks.chunkList[ 3 ].lineCount() )
		self.assertEqual( 99, self.chunks.chunkList[ 3 ].lastIndex() )

		self.checkChunkCount( 2 )

	def testDuplicateCurrentLine_smallFile( self ):
		# Given
		smallFileName = FileChunk.CHUNK_PATH + '/SmallFile.txt'
		with open( smallFileName, 'w', encoding = 'utf-8' ) as f:
			f.write( "Line 1\n" )
			f.write( "Line 2\n" )
		self.chunks = FileChunks( smallFileName )

		# When
		self.chunks.setCurrentLineIndex( 1 )
		self.chunks.duplicateCurrentLine()

		# Then
		self.assertEqual( 3, self.chunks.lineCount() )
		self.assertEqual( 1, len( self.chunks.chunkList ) )

		self.assertEqual( 0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 3, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 2, self.chunks.chunkList[ 0 ].lastIndex() )

		self.checkChunkCount( 1 )

	def testDeleteCurrentLine( self ):
		# Given
		self.chunks.setCurrentLineIndex( 5 )
		self.assertEqual( 'List 1', self.chunks.getCurrentLine().text )
		self.chunks.setCurrentLineIndex( 6 )
		self.assertEqual( 'The birch canoe slid on the smooth planks.', self.chunks.getCurrentLine().text )

		# When
		self.chunks.setCurrentLineIndex( 5 )
		self.chunks.deleteCurrentLine()

		# Then
		self.assertEqual( 98, self.chunks.lineCount() )
		self.chunks.setCurrentLineIndex( 5 )
		self.assertEqual( 'The birch canoe slid on the smooth planks.', self.chunks.getCurrentLine().text )

		# This chunk should have got shorter
		self.assertEqual(  0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 48, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 47, self.chunks.chunkList[ 0 ].lastIndex() )

		# Other chunks' line numbers should have adjusted
		self.assertEqual( 48, self.chunks.chunkList[ 1 ].startIndex )
		self.assertEqual( 22, self.chunks.chunkList[ 1 ].lineCount() )
		self.assertEqual( 69, self.chunks.chunkList[ 1 ].lastIndex() )

		self.assertEqual( 70, self.chunks.chunkList[ 2 ].startIndex )
		self.assertEqual( 18, self.chunks.chunkList[ 2 ].lineCount() )
		self.assertEqual( 87, self.chunks.chunkList[ 2 ].lastIndex() )

		self.assertEqual( 88, self.chunks.chunkList[ 3 ].startIndex )
		self.assertEqual( 10, self.chunks.chunkList[ 3 ].lineCount() )
		self.assertEqual( 97, self.chunks.chunkList[ 3 ].lastIndex() )

		self.checkChunkCount( 2 )

	def testDeleteCurrentLine_smallFile( self ):
		# Given
		smallFileName = FileChunk.CHUNK_PATH + '/SmallFile.txt'
		with open( smallFileName, 'w', encoding = 'utf-8' ) as f:
			f.write( "Line 1\n" )
			f.write( "Line 2\n" )
		self.chunks = FileChunks( smallFileName )

		# When
		self.chunks.setCurrentLineIndex( 1 )
		self.chunks.deleteCurrentLine()

		# Then
		self.assertEqual( 1, self.chunks.lineCount() )
		self.assertEqual( 1, len( self.chunks.chunkList ) )

		self.assertEqual( 0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 1, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 0, self.chunks.chunkList[ 0 ].lastIndex() )

		self.checkChunkCount( 1 )

	def testMoveUp_withinChunk( self ):
		# Given
		self.chunks.setCurrentLineIndex( 5 )
		self.assertEqual( 'List 1', self.chunks.getCurrentLine().text )
		self.chunks.setCurrentLineIndex( 6 )
		self.assertEqual( 'The birch canoe slid on the smooth planks.', self.chunks.getCurrentLine().text )

		# When
		self.chunks.moveUp()

		# Then
		self.assertEqual( 5, self.chunks.getCurrentLineIndex() )
		self.assertEqual( 'The birch canoe slid on the smooth planks.', self.chunks.getCurrentLine().text )
		self.chunks.setCurrentLineIndex( 6 )
		self.assertEqual( 'List 1', self.chunks.getCurrentLine().text )

		# The chunk indexes should not have changed
		self.assertEqual(  0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 49, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 48, self.chunks.chunkList[ 0 ].lastIndex() )

		self.checkChunkCount( 2 )

	def testMoveUp_acrossChunkBoundary( self ):
		# Given
		lineGoingUp = "2. 'All the world's a stage, and all the men and women merely players. They have their exits and their entrances; And one man in his time plays many parts.' (As You Like It, Act 2, Scene 7)"
		lineGoingDown = "1. 'To be, or not to be: that is the question' (Hamlet, Act 3, Scene 1)"
		self.chunks.setCurrentLineIndex( 48 )
		self.assertEqual( lineGoingDown, self.chunks.getCurrentLine().text )
		self.chunks.setCurrentLineIndex( 49 )
		self.assertEqual( lineGoingUp, self.chunks.getCurrentLine().text )

		# When
		self.chunks.moveUp()

		# Then
		self.assertEqual( 48, self.chunks.getCurrentLineIndex() )
		self.assertEqual( lineGoingUp, self.chunks.getCurrentLine().text )
		self.chunks.setCurrentLineIndex( 49 )
		self.assertEqual( lineGoingDown, self.chunks.getCurrentLine().text )

		# The chunks indexes should not have changed
		self.assertEqual(  0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 49, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 48, self.chunks.chunkList[ 0 ].lastIndex() )

		self.assertEqual( 49, self.chunks.chunkList[ 1 ].startIndex )
		self.assertEqual( 22, self.chunks.chunkList[ 1 ].lineCount() )
		self.assertEqual( 70, self.chunks.chunkList[ 1 ].lastIndex() )

		self.checkChunkCount( 2 )

	def testMoveDown_withinChunk( self ):
		# Given
		self.chunks.setCurrentLineIndex( 6 )
		self.assertEqual( 'The birch canoe slid on the smooth planks.', self.chunks.getCurrentLine().text )
		self.chunks.setCurrentLineIndex( 5 )
		self.assertEqual( 'List 1', self.chunks.getCurrentLine().text )

		# When
		self.chunks.moveDown()

		# Then
		self.assertEqual( 6, self.chunks.getCurrentLineIndex() )
		self.assertEqual( 'List 1', self.chunks.getCurrentLine().text )
		self.chunks.setCurrentLineIndex( 5 )
		self.assertEqual( 'The birch canoe slid on the smooth planks.', self.chunks.getCurrentLine().text )

		# The chunk indexes should not have changed
		self.assertEqual(  0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 49, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 48, self.chunks.chunkList[ 0 ].lastIndex() )

		self.checkChunkCount( 2 )

	def testMoveDown_acrossChunkBoundary( self ):
		# Given
		lineGoingDown = "1. 'To be, or not to be: that is the question' (Hamlet, Act 3, Scene 1)"
		lineGoingUp = "2. 'All the world's a stage, and all the men and women merely players. They have their exits and their entrances; And one man in his time plays many parts.' (As You Like It, Act 2, Scene 7)"
		self.chunks.setCurrentLineIndex( 49 )
		self.assertEqual( lineGoingUp, self.chunks.getCurrentLine().text )
		self.chunks.setCurrentLineIndex( 48 )
		self.assertEqual( lineGoingDown, self.chunks.getCurrentLine().text )

		# When
		self.chunks.moveDown()

		# Then
		self.assertEqual( 49, self.chunks.getCurrentLineIndex() )
		self.assertEqual( lineGoingDown, self.chunks.getCurrentLine().text )
		self.chunks.setCurrentLineIndex( 48 )
		self.assertEqual( lineGoingUp, self.chunks.getCurrentLine().text )

		# The chunks indexes should not have changed
		self.assertEqual(  0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 49, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 48, self.chunks.chunkList[ 0 ].lastIndex() )

		self.assertEqual( 49, self.chunks.chunkList[ 1 ].startIndex )
		self.assertEqual( 22, self.chunks.chunkList[ 1 ].lineCount() )
		self.assertEqual( 70, self.chunks.chunkList[ 1 ].lastIndex() )

		self.checkChunkCount( 2 )

	def testChunkMerge_1( self ):
		# When
		self.chunks.setCurrentLineIndex( 89 )
		for index in range( 89, 94 ):
			self.chunks.deleteCurrentLine()

		# Then
		self.assertEqual( 3, len( self.chunks.chunkList ) )

		self.assertEqual(  0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 49, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 48, self.chunks.chunkList[ 0 ].lastIndex() )

		self.assertEqual( 49, self.chunks.chunkList[ 1 ].startIndex )
		self.assertEqual( 22, self.chunks.chunkList[ 1 ].lineCount() )
		self.assertEqual( 70, self.chunks.chunkList[ 1 ].lastIndex() )

		self.assertEqual( 71, self.chunks.chunkList[ 2 ].startIndex )
		self.assertEqual( 23, self.chunks.chunkList[ 2 ].lineCount() )
		self.assertEqual( 93, self.chunks.chunkList[ 2 ].lastIndex() )

		self.checkChunkCount( 2 )

	def testChunkMerge_2( self ):
		# When
		self.chunks.setCurrentLineIndex( 0 )
		for index in range( 0, 40 ):
			self.chunks.deleteCurrentLine()

		# Then
		self.assertEqual( 3, len( self.chunks.chunkList ) )

		self.assertEqual(  0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 31, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 30, self.chunks.chunkList[ 0 ].lastIndex() )

		self.assertEqual( 31, self.chunks.chunkList[ 1 ].startIndex )
		self.assertEqual( 18, self.chunks.chunkList[ 1 ].lineCount() )
		self.assertEqual( 48, self.chunks.chunkList[ 1 ].lastIndex() )

		self.assertEqual( 49, self.chunks.chunkList[ 2 ].startIndex )
		self.assertEqual( 10, self.chunks.chunkList[ 2 ].lineCount() )
		self.assertEqual( 58, self.chunks.chunkList[ 2 ].lastIndex() )

		self.checkChunkCount( 2 )

	def testChunkMerge_3( self ):
		# When
		self.chunks.setCurrentLineIndex( 49 )
		for index in range( 0, 13 ):
			self.chunks.deleteCurrentLine()

		# Then
		self.assertEqual( 3, len( self.chunks.chunkList ) )

		self.assertEqual(  0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 49, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 48, self.chunks.chunkList[ 0 ].lastIndex() )

		self.assertEqual( 49, self.chunks.chunkList[ 1 ].startIndex )
		self.assertEqual( 27, self.chunks.chunkList[ 1 ].lineCount() )
		self.assertEqual( 75, self.chunks.chunkList[ 1 ].lastIndex() )

		self.assertEqual( 76, self.chunks.chunkList[ 2 ].startIndex )
		self.assertEqual( 10, self.chunks.chunkList[ 2 ].lineCount() )
		self.assertEqual( 85, self.chunks.chunkList[ 2 ].lastIndex() )

		self.checkChunkCount( 2 )

	def testSmallFile( self ):
		#Given
		smallFileName = FileChunk.CHUNK_PATH + '/SmallFile.txt'
		with open( smallFileName, 'w', encoding = 'utf-8' ) as f:
			f.write( "Line 1\n" )
			f.write( "Line 2\n" )
			f.write( "Line 3\n" )

		# When
		self.chunks = FileChunks( smallFileName )

		# Then
		self.assertEqual( 1, len( self.chunks.chunkList ) )
		self.assertEqual( 3, self.chunks.lineCount() )

		self.assertEqual( 0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 3, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 2, self.chunks.chunkList[ 0 ].lastIndex() )

		self.assertEqual(  0, self.chunks.topChunkIndex    )
		self.assertEqual( -1, self.chunks.bottomChunkIndex )

		self.checkChunkCount( 1 )

		# And when
		self.chunks.deleteCurrentLine()

		# Then
		self.assertEqual( 1, len( self.chunks.chunkList ) )
		self.assertEqual( 2, self.chunks.lineCount() )

		self.assertEqual( 0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 2, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 1, self.chunks.chunkList[ 0 ].lastIndex() )

		self.assertEqual(  0, self.chunks.topChunkIndex    )
		self.assertEqual( -1, self.chunks.bottomChunkIndex )

		self.checkChunkCount( 1 )

	def testEmptyFile( self ):
		# Given
		emptyFileName = FileChunk.CHUNK_PATH + '/EmptyFile.txt'
		with open( emptyFileName, 'w', encoding = 'utf-8' ) as f:
			pass

		# When
		self.chunks = FileChunks( emptyFileName )

		# Then
		self.assertEqual( 1, len( self.chunks.chunkList ) )

		self.assertEqual(  0, self.chunks.topChunkIndex    )
		self.assertEqual( -1, self.chunks.bottomChunkIndex )

		self.assertEqual( 0, self.chunks.getCurrentLineIndex() )
		self.assertEqual( 0, self.chunks.lineCount() )

		self.checkChunkCount( 0 )

	def testInsert( self ):
		# Given
		self.assertEqual( 0, self.chunks.getCurrentLineIndex() )
		oldLineZeroText = self.chunks.getCurrentLine().text

		# When
		self.chunks.insertLine( 'xyz' )

		# Then
		self.assertEqual( 0, self.chunks.getCurrentLineIndex() )

		self.assertEqual( 'xyz', self.chunks.getCurrentLine().text )
		self.chunks.setCurrentLineIndex( 1 )
		self.assertEqual( oldLineZeroText, self.chunks.getCurrentLine().text )

		self.assertEqual( 4, len( self.chunks.chunkList ) )

		self.assertEqual(  0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 50, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 49, self.chunks.chunkList[ 0 ].lastIndex() )

		self.assertEqual( 50, self.chunks.chunkList[ 1 ].startIndex )
		self.assertEqual( 22, self.chunks.chunkList[ 1 ].lineCount() )
		self.assertEqual( 71, self.chunks.chunkList[ 1 ].lastIndex() )

		self.assertEqual( 72, self.chunks.chunkList[ 2 ].startIndex )
		self.assertEqual( 18, self.chunks.chunkList[ 2 ].lineCount() )
		self.assertEqual( 89, self.chunks.chunkList[ 2 ].lastIndex() )

		self.assertEqual( 90, self.chunks.chunkList[ 3 ].startIndex )
		self.assertEqual( 10, self.chunks.chunkList[ 3 ].lineCount() )
		self.assertEqual( 99, self.chunks.chunkList[ 3 ].lastIndex() )

		self.checkChunkCount( 2 )

	def testInsert_emptyFile( self ):
		# Given
		emptyFileName = FileChunk.CHUNK_PATH + '/EmptyFile.txt'
		with open( emptyFileName, 'w', encoding = 'utf-8' ) as f:
			pass
		self.chunks = FileChunks( emptyFileName )
		self.assertEqual( 0, self.chunks.getCurrentLineIndex() )
		self.assertEqual( 0, self.chunks.lineCount() )

		# When
		self.chunks.insertLine( 'Only line' )

		# Then
		self.assertEqual( 0, self.chunks.getCurrentLineIndex() )
		self.assertEqual( 1, self.chunks.lineCount() )
		self.assertEqual( 1, len( self.chunks.chunkList ) )

		self.assertEqual( 'Only line', self.chunks.getCurrentLine().text )

		self.assertEqual( 0, self.chunks.chunkList[ 0 ].startIndex )
		self.assertEqual( 1, self.chunks.chunkList[ 0 ].lineCount() )
		self.assertEqual( 0, self.chunks.chunkList[ 0 ].lastIndex() )

		self.checkChunkCount( 1 )

	def testClose( self ):
		# When
		self.chunks.close()

		# Then
		self.assertEqual( 0, len( self.chunks.chunkList ) )
		self.assertEqual( 0, len( listdir( FileChunk.CHUNK_PATH ) ) )

		self.checkChunkCount( 0 )

if __name__ == '__main__':
	unittest.main()
