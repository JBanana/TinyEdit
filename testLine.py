import unittest
from FileChunks import Line

class LineTest( unittest.TestCase ):
	def testCtor( self ):
		# When
		line = Line( 'xyz' )

		# Then
		self.assertEqual( 'xyz', line.text )

		# And when
		line = Line( 'xyz\n' )

		# Then
		self.assertEqual( 'xyz', line.text )

	def testGetText( self ):
		# Given
		line = Line( '0123456789abcdef' )

		# Then
		self.assertEqual( ' 0123456789>', line.getText(  0 ) )
		self.assertEqual( '<123456789a>', line.getText(  1 ) )
		self.assertEqual( '<abcdef',      line.getText( 10 ) )
		self.assertEqual( '<f',           line.getText( 15 ) )
		self.assertEqual( '<',            line.getText( 16 ) )
		self.assertEqual( '<',            line.getText( 99 ) )
		self.assertEqual( '0123456789abcdef', line.text )

	def testSetText( self ):
		# Given
		line = Line( 'previous' )

		# When
		line.setText( 'a new value' )

		# Then
		self.assertEqual( 'a new value', line.text )
		self.assertEqual( '<value', line.getText( 6 ) )

if __name__ == '__main__':
	unittest.main()
